<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Supprimer un mot</title>
  <link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>

  <ul>
    <li class="head"><a href="mot.php">Liste de mots</a></li>
    <li class="head"><a href="nom.php">Liste des joueurs</a></li>
  </ul>

<?php

$handle = mysqli_connect("localhost","root","aurelie6569","Pendu App");

if (isset($_GET["ID"]) && is_numeric($_GET["ID"])) {
  $query = "SELECT * FROM Mots WHERE ID = " . $_GET["ID"];
  $result = mysqli_query($handle,$query);
  if ($result->num_rows > 0) {
      $line = mysqli_fetch_array($result);
      $mot_a_supprimer = $line["Mots"];
      $query = "DELETE FROM Mots WHERE ID = " . $_GET["ID"];
      $result = mysqli_query($handle,$query);

      if ($handle->affected_rows > 0) {
          echo "Mot " . $mot_a_supprimer . " supprimé<br>\n";
      }
      else {
          echo "Le mot " . $mot_a_supprimer . " existe mais la suppression n’a pas marché<br>\n";
      }
  }
  else {
      echo "Le mot demandé n’existe pas<br>\n";
  }
}
else {
  echo "Veuillez indiquer la variable id ou vérifier qu’il
      s’agit bien d’un nombre";
}
  echo "<a href=\"mot.php\">retour</a>";
?>

</body>
</html>
