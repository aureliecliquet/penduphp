<!doctype html>
<html>
<head>
	<title>Modification d'un joueur</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <ul>
      <li class="head"><a href="nom.php">Liste des joueurs</a></li>
      <li class="head"><a href="mot.php">Liste de mots</a></li>
    </ul>

    <form action="update_nom.php" method="GET">
      <label for="Nom">Modifier le nom du joueur</label>
      <input name="Nom" type="text" placeholder="Votre nouveau nom">
      <input type="hidden" name="ID" value="<?php echo $_GET["ID"]; ?>" >
      <input type="submit">
    </form>

<a href="nom.php">Retour à la liste des joueurs</a>
</body>
</html>
